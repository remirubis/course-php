<?php

function maskify(string $cc): string
{
  if ($cc === '') return '';
  $pattern = '/[0-9]/';
  $hidden = preg_replace($pattern, '#', $cc);
  $hidden = substr($hidden, 1, -4);
  
  return $cc[0] . $hidden . substr($cc, -4);
}