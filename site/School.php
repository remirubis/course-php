<?php

class School
{
  public function __construct()
  {
    $this->school = [];
  }

  public function add(string $name, int $grade): String
  {
    echo "Add $name to grade $grade.<br />";
    $exist = false;
    foreach ($this->school as $uGrade) {
      if (in_array($name, $uGrade)) {
        $exist = true;
      }
    }
    if(!$exist) {
      $this->school[$grade][] = $name;
      return  "OK";
    } else return "Already exist";
  }

  public function grade($grade) : array
  {
    if (!isset($this->school[$grade])) return [];
    return $this->school[$grade];
  }

  public function studentsByGradeAlphabetical() : array
  {
    ksort($this->school);
    foreach ($this->school as $key => $data) {
      sort($this->school[$key]);
    }
    return $this->school;
  }
}
