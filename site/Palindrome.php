<?php

function smallest(int $min, int $max): array
{
  $palindromes = palindromes($min, $max, 'smallest');
  $key = array_key_first($palindromes);
  return [$key, $palindromes[$key]];
}

function largest(int $min, int $max): array
{
  $palindromes = palindromes($min, $max, 'largest');
  $key = array_key_last($palindromes);
  return [$key, $palindromes[$key]];
}

function palindromes(int $min, int $max, string $type): array
{
  if ($min >= $max) throw new Exception();
  $current = $type === 'smallest' ? $max * $max : $min * $min;
  $palindromes = [];
  for ($i = $min; $i <= $max; $i++) {
    for ($j = $i; $j <= $max; $j++) {
      $product = $i * $j;
      if (
        ($type === 'smallest' && $product > $current) ||
        ($type === 'largest' && $product < $current)
      ) {
        continue;
      }
      if (is_palindrome($product)) {
        $current = $product;
        $palindromes[$product] = $palindromes[$product] ?? [];
        $palindromes[$product][] = [$i, $j];
      }
    }
  }
  if (sizeof($palindromes) === 0) throw new Exception();
  ksort($palindromes);
  return $palindromes;
}

function is_palindrome(int $number): bool
{
  return $number === (int)implode(array_reverse(str_split((string)$number)));
}
