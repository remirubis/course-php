<?php

class Robot
{
  public function __construct(?string $name = null) {
    if (!isset($GLOBALS['robots'])) {
      global $robots;
      $robots = [];
    }
    $this->robots = $GLOBALS['robots'];
    $this->setName($name);
  }

  public function setName($name): void
  {
    if (!$name || $this->isAlreadyExist($name))
      $this->reset();
    else {
      $this->name = $name;
      array_push($this->robots, $name);
      $GLOBALS['robots'] = $this->robots;
    }
  }

  public function getName(): string
  {
    return $this->name;
  }

  public function isAlreadyExist($name) : bool {
    return in_array($name, $this->robots);
  }

  public function reset(): void
  {
    $this->robots = $GLOBALS['robots'];
    $text = substr(str_shuffle(str_repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 2)), 0, 2);
    $randomInt = random_int(0, 999);
    if($randomInt < 10) {
      $randomInt = 00 . $randomInt;
    } if($randomInt < 100) {
      $randomInt = 0 . $randomInt;
    } else $randomInt;
    $name = $text . $randomInt;

    $this->setName($name);
  }
}
