<?php

require __DIR__ . '/helloWorld.php';
require __DIR__ . '/reverseString.php';
require __DIR__ . '/Robot.php';
require __DIR__ . '/maskify.php';
require __DIR__ . '/Clock.php';
require __DIR__ . '/School.php';
require __DIR__ . '/Matrix.php';
require __DIR__ . '/Palindrome.php';

function var_table ($array) {
    return '<pre>' . var_export($array, true) . '</pre>';
}

try {
    echo 'Current PHP version: ' . phpversion();
    echo '<br />';

    $host = 'db';
    $dbname = 'database';
    $user = 'user';
    $pass = 'pass';
    $dsn = "mysql:host=$host;dbname=$dbname;charset=utf8";
    $conn = new PDO($dsn, $user, $pass);

    echo 'Database connected successfully';
    echo '<br />';
} catch (\Throwable $t) {
    echo 'Error: ' . $t->getMessage();
    echo '<br />';
}

// Exo 1
echo helloWorld();
echo '<br />';

// Exo 2
echo reverseString('cool');
echo '<br />';

// Exo 3
$robot = new Robot();
echo $robot->getName();
echo '<br />';

$robot2 = new Robot($robot->getName());
echo $robot2->getName();
echo '<br />';

// Exo 4
echo maskify('123456789012'). '<br />';
echo maskify('1234-5678-9012') . '<br />';
echo maskify('') . '<br />';

// Exo 5
$clock = new Clock(8);
echo $clock . '<br />';
$clock->add(10);
echo $clock . '<br />';
$clock->sub(5);
echo $clock . '<br />';

// Exo 6
$school = new School();
$school->add('Zola', 1);
$school->add('Rémi', 1);
$school->add('Arachide', 1);
$school->add('Rémi', 12);
$school->add('Pierre', 1);
$school->add('Alexandre', 6);
$school->add('Rémi', 2);
// echo var_table($school->grade(4));
$school->studentsByGradeAlphabetical();
// echo var_table($school->school);

// Exo 7
$matrix = new Matrix("1 2 3 4 8\n4 5 6 4 7\n7 8 0 4 6\n7 8 0 4 5");
// echo var_table($matrix->getRow(1));
// echo var_table($matrix->getColumn(1));

// Exo 8
[$value, $factors] = smallest(10, 99);
// echo "Min value: $value";
// echo var_table($factors);