<?php


class Matrix
{ 
  public function __construct(string $matrix)
  {
    $this->matrix = $matrix;
  }

  public function getRow($row) : array
  {
    $row = explode(PHP_EOL, $this->matrix)[$row - 1];
    return explode(' ', $row);
  }

  public function getColumn($column) : array
  {
    $col = [];
    $rows = explode(PHP_EOL, $this->matrix);
    foreach ($rows as $lineNb) {
      $row = explode(' ', $lineNb);
      foreach ($row as $key => $val) {
        $col[$key][] = $val;
      }
    }
    if (!isset($col[$column - 1])) return [];
    return $col[$column - 1];
  }
}
